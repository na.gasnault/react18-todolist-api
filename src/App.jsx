import { useState, useEffect } from "react";

import "./App.css";
import TodoList from "./components/TodoList";
import axios from "axios";

const API_URL = "https://jsonplaceholder.typicode.com/todos";

function App() {
  const [todos, setTodos] = useState([]);

  // TODO install and import axios, create an useEffect hook, and call the API

  //Utilisation de la requette GET pour afficher les données de mon lien stocké dans API_URL
  //-Utilisation de l'hook useEffect pour effectuer une action sur le composant
  useEffect(() => {
    //- .then permet de recuperer toutes les données (alors)
    //-response contient un objet javascript (ici c'est la liste des choses à faire)
    //- lorque axios appel l'api il le stock dans response
    axios.get(API_URL).then((response) => {
      setTodos(response.data) //
    })
  }, []); //-Le tableau vide [] en deuxième argument signifie que cet effet s'exécutera uniquement lors du montage initial du composant.


  return (
    <div className="App">
      <TodoList todos={todos} />
    </div>
  );
}

export default App;
